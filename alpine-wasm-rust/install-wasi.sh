#!/bin/sh

WASI_VERSION=22
WASI_VERSION_FULL=$WASI_VERSION.0
WASI_BASE=wasi-sdk-$WASI_VERSION_FULL
wget -P /tmp https://github.com/WebAssembly/wasi-sdk/releases/download/wasi-sdk-${WASI_VERSION}/${WASI_BASE}-linux.tar.gz

mkdir -p $WASI_SDK_PATH
tar xvf /tmp/${WASI_BASE}-linux.tar.gz -C $WASI_SDK_PATH --strip-components 1
rm /tmp/${WASI_BASE}-linux.tar.gz
